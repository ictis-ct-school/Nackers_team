class Convertor:
    def __init__(self):
        self.rub = 0

        self.outp = 0

        self.__k_usd = 61.77
        self.__k_evr = 64.99
        self.__k_uan = 8.78
        self.nam = [5000, 2000, 1000, 500, 200, 100, 50, 10, 5, 2, 1]

    def runner(self, command, rub):
        self.rub = rub
        if command[0] == 'convert':
            if command[2] == 'usd':
                self.outp = self.rub / self.__k_usd
            if command[2] == 'evr':
                self.outp = self.rub / self.__k_evr
            if command[2] == 'uan':
                self.outp = self.rub / self.__k_uan

        if command[0] == 'parse':
            for i in range(11):
                if self.nam[i] <= self.rub:
                    lk = self.rub  // self.nam[i]
                    if lk != 0:
                        print(f"{self.nam[i]} : {lk}")
                        self.rub -= self.nam[i]*lk
    
con = Convertor()
pk = input("$ ").split()

if pk[0] == "convert":
    con.runner(pk[:3], int(pk[3]))
    print('%.2f' % con.rub)

if pk[0] == "parse":
    con.runner([pk[0]], int(pk[1]))
